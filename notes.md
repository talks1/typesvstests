
Types are like unit tests
Type constrain the system in a way that unit tests do.

WIth types the compiler can verify for us.
WIth unit tests we have to run them separately.

Unit tests will always be more comprehensive tests than type tests.


Tests represent intent outside of the code
Types represent intent inside the code

Changes are made by people who arent the original authors and dont have a comprehensive understand for what the types represent.
Ideally we figure out the code we want to change.  Write tests to ensure what it does is maintained as we change it and then make the changes.

The problem with types is that we have to change the types to make the changes and dont have any way to 


Another problem with types is that you need a compiler to be able to effective change code.
When you have a production issue it sometimes is very helpful to be able to make local changes...when there is not likely to be a type compiler available.


[AHA principle - Avoid hasty abstractions](https://kentcdodds.com/blog/aha-programming)
I fear that a lot of types are hasty abstractions.

[This talk describes how we tend to modify by adding conditional complexity and we must avoid hasty abstractions when refactoring to get to useful abstractions](https://youtu.be/8bZh5LMaSmE)


This talk about 'solving problems the closure way' demonstrates (with javascript) how to refact procedural code into functional code[https://www.youtube.com/watch?v=vK1DazRK_a0]
